const {Garbage, Gio, GLib, GObject} = imports.gi;
const System = imports.system;

System.gc();  // collect any garbage from the GI overrides

const App = GObject.registerClass({
    GTypeName: 'App',
}, class App extends Gio.Application {
    _init(props = {}) {
        Object.assign(props, {
            applicationId: 'name.ptomato.Garbage',
        });
        super._init(props);
        this.__heapgraph_name = 'App';
    }

    vfunc_activate() {
        this._window = new Garbage.Window();
        const label2 = new Garbage.Label2();
        const button = new Garbage.Button();
        const box2 = new Garbage.Box2();
        box2.add(label2);
        box2.add(button);
        const label1 = new Garbage.Label1();
        const box1 = new Garbage.Box1();
        box1.add(label1);
        box1.add(box2);
        this._window.add(box1);

        // Force all the objects to use toggle refs / have expandos
        label1.__heapgraph_name = 'Label1';
        button.__heapgraph_name = 'Button';
        box1.__heapgraph_name = 'Box1';
        label2.__heapgraph_name = 'Label2';
        box2.__heapgraph_name = 'Box2';
        this._window.__heapgraph_name = 'Window';

        // Impede efforts to collect the tree all at once
        // this._window._button = button;
        // button._button = button;

        System.dumpHeap('0--start.heap');
        Garbage.dump('0--start.post');

        // Let the lexical bindings in this function go out of scope before we
        // do our next dump
        this.hold();
        GLib.idle_add(GLib.PRIORITY_LOW, this._dump.bind(this));
    }

    _dump() {
        // this._window.destroy();
        this._window = null;

        System.dumpHeap('1--beforegc.heap');
        Garbage.dump('1--beforegc.post');
        System.gc();
        System.dumpHeap('2--gc.heap');
        Garbage.dump('2--gc.post');
        System.gc();
        System.dumpHeap('3--gc.heap');
        Garbage.dump('3--gc.post');
        System.gc();
        System.dumpHeap('4--gc.heap');
        Garbage.dump('4--gc.post');
        System.gc();
        System.dumpHeap('5--gc.heap');
        Garbage.dump('5--gc.post');

        this.release();
        return GLib.SOURCE_REMOVE;
    }
});
const theApp = new App();
theApp.run([System.programInvocationName].concat(ARGV));
