# Garbage #

This is a controlled environment with toy GObjects for testing the fix
for the GJS garbage collection problems (GNOME/gjs#217).

It creates diagrams of GObjects and JS Objects that refer to each other,
such as this one:

![Diagram example](example.png "Diagram example")

## How to run ##

If you want to test this for yourself, here's what I suggest.

First install `xdot` to view the graphs.

Clone this repository, and also GNOME/gjs.

Make a link inside `gjs` to this repository.

```sh
cd gjs
ln -s ../garbage
```

Edit `garbage.js` to create the test environment you want.

Edit `run` as necessary.
For example, use `--toggle` to generate graphs that assume the toggle
reference scheme on GJS master.

```
cd garbage
./run
```

You will see heap dumps, files with postprocessing information for the
GObject references, and .dot graphs appear in the directory, and the
script will view each graph with `xdot`.
