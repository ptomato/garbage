#include <stdio.h>

#include <gio/gio.h>

#include "garbage.h"

#define GOBJECT_STYLE "color=deepskyblue4, shape=square, style=\"solid\""
#define C_REFERENCE_STYLE "color=deepskyblue3, fontcolor=gray25, style=\"bold\""

G_DEFINE_QUARK(gjs::private, gjs_private)

static GSList* objects = NULL;

struct _GarbageCan {
    GObject parent_instance;
    GSList* objects;
};
G_DEFINE_TYPE(GarbageCan, garbage_can, G_TYPE_OBJECT)

static void garbage_can_dispose(GObject* object) {
    GarbageCan* self = GARBAGE_CAN(object);
    g_slist_free_full(self->objects, g_object_unref);
    self->objects = NULL;
    G_OBJECT_CLASS(garbage_can_parent_class)->dispose(object);
}

static void garbage_can_finalize(GObject* object) {
    GarbageCan* self = GARBAGE_CAN(object);
    g_slist_free_full(self->objects, g_object_unref);
    self->objects = NULL;
    objects = g_slist_remove(objects, object);
    G_OBJECT_CLASS(garbage_can_parent_class)->finalize(object);
}

static void garbage_can_class_init(GarbageCanClass* klass) {
    GObjectClass* object_class = G_OBJECT_CLASS(klass);
    /* (un)comment the following line to switch between dropping references on
     * dispose or finalize. It doesn't matter for regular objects, although for
     * GtkWidgets it definitely does, because destroy() will run dispose. */
    object_class->dispose = garbage_can_dispose;
    object_class->finalize = garbage_can_finalize;
}

static void garbage_can_init(GarbageCan* self) {
    objects = g_slist_prepend(objects, self);
}

void garbage_can_add(GarbageCan* self, GObject* garbage) {
    self->objects = g_slist_prepend(self->objects, g_object_ref(garbage));
}

void garbage_can_destroy(GarbageCan* self) {
    g_slist_foreach(self->objects, (GFunc) garbage_can_destroy, NULL);
    g_object_run_dispose(G_OBJECT(self));
}

void dump_gobject(GObject* obj, FILE* fp) {
    void* priv = g_object_get_qdata(obj, gjs_private_quark());
    g_autofree char* node = g_strdup_printf(
        "  node [label=\"%s (%u)\\ngobj@%p\\npriv@%p\", %s] g%p;\n",
        g_type_name(G_OBJECT_TYPE(obj)), obj->ref_count, obj, priv,
        GOBJECT_STYLE, obj);
    fwrite(node, 1, strlen(node), fp);
}

void dump_edges(GarbageCan* self, FILE* fp) {
    for (GSList* iter = self->objects; iter; iter = g_slist_next(iter)) {
        g_autofree char* edge =
            g_strdup_printf("  g%p -> g%p [label=\"C reference\", %s];\n",
                            self, iter->data, C_REFERENCE_STYLE);
        fwrite(edge, 1, strlen(edge), fp);
    }
}

void garbage_dump(const char* filename) {
    FILE* fp = fopen(filename, "w");

    GApplication* app = g_application_get_default();
    if (app)
        dump_gobject(G_OBJECT(app), fp);

    g_slist_foreach(objects, (GFunc) dump_gobject, fp);
    g_slist_foreach(objects, (GFunc) dump_edges, fp);

    fclose(fp);
}

struct _GarbageWindow {
    GarbageCan parent_instance;
};
G_DEFINE_TYPE(GarbageWindow, garbage_window, GARBAGE_TYPE_CAN)
static void garbage_window_class_init(GarbageWindowClass* klass) {}
static void garbage_window_init(GarbageWindow* self) {}

struct _GarbageBox1 {
    GarbageCan parent_instance;
};
G_DEFINE_TYPE(GarbageBox1, garbage_box1, GARBAGE_TYPE_CAN)
static void garbage_box1_class_init(GarbageBox1Class* klass) {}
static void garbage_box1_init(GarbageBox1* self) {}

struct _GarbageLabel1 {
    GarbageCan parent_instance;
};
G_DEFINE_TYPE(GarbageLabel1, garbage_label1, GARBAGE_TYPE_CAN)
static void garbage_label1_class_init(GarbageLabel1Class* klass) {}
static void garbage_label1_init(GarbageLabel1* self) {}

struct _GarbageBox2 {
    GarbageCan parent_instance;
};
G_DEFINE_TYPE(GarbageBox2, garbage_box2, GARBAGE_TYPE_CAN)
static void garbage_box2_class_init(GarbageBox2Class* klass) {}
static void garbage_box2_init(GarbageBox2* self) {}

struct _GarbageLabel2 {
    GarbageCan parent_instance;
};
G_DEFINE_TYPE(GarbageLabel2, garbage_label2, GARBAGE_TYPE_CAN)
static void garbage_label2_class_init(GarbageLabel2Class* klass) {}
static void garbage_label2_init(GarbageLabel2* self) {}

struct _GarbageButton {
    GarbageCan parent_instance;
};
G_DEFINE_TYPE(GarbageButton, garbage_button, GARBAGE_TYPE_CAN)
static void garbage_button_class_init(GarbageButtonClass* klass) {}
static void garbage_button_init(GarbageButton* self) {}
