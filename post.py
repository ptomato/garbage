#!/usr/bin/env python3

import argparse
import re

parser = argparse.ArgumentParser()
parser.add_argument('dotfile', metavar='FILE')
parser.add_argument('postfile', metavar='FILE')
parser.add_argument('--verbose', action='store_true',
                    help='Show pointer addresses in the nodes')
parser.add_argument('--toggles', action='store_true',
                    help='Use old scheme of toggle references')
args = parser.parse_args()

UNREACHABLE_STYLE = 'color=orange, shape=circle, style="dashed"'
UNREACHABLE_EDGE_STYLE = 'color=mediumorchid3, fontcolor=gray25, style="dashed"'
JS_GOBJECT_EDGE_STYLE = 'color=mediumorchid3, fontcolor=gray25, style="solid"'
C_REFERENCE_STYLE = 'color=deepskyblue3, fontcolor=gray25, style="bold"'

# "_button" is for a special case where we want to illustrate something
# particular in the talk
special_case_re = re.compile(r'q(0x[a-fA-F0-9]+) -> q(0x[a-fA-F0-9]+) \[label="_button"')
proxy_re = re.compile(r'q(0x[a-fA-F0-9]+) -> q(0x[a-fA-F0-9]+) \[label="proxy target"')
jsedge_re = re.compile(r'jsobj@(0x[a-fA-F0-9]+).*priv@(0x[a-fA-F0-9]+)')
cedge_re = re.compile(r'gobj@(0x[a-fA-F0-9]+).*priv@(0x[a-fA-F0-9]+)')
priv_addr_re = re.compile(r'priv@(0x[a-fA-F0-9]+)')
jsaddr_re = re.compile(r'q(0x[a-fA-F0-9]+);')

lines = []
privs = {}  # dict of jsprivate address : jsobject address
proxies = {}  # dict of proxy target address : proxy address
edge_lines = []
rank_lines = []
# strip out unreachable nodes, and only add them in if they relate to a GObject
unreachable_lines = {}  # dict of jsprivate address : (jsobject address, graphviz line)
unreachable_maybe_readd = {}  # dict of jsobject address : graphviz line

defaults = [
    '  graph [fontname="Cantarell", nodesep="0.75", ranksep="1"]\n',
    '  node [fontname="Cantarell", penwidth=2]\n',
    '  edge [fontname="Cantarell", penwidth=1, arrowsize=2]\n',
]


def format_js_gobject_association(gobj_addr, priv_addr, lastref):
    jsaddr = privs.get(priv_addr, None)
    if jsaddr is None:
        jsaddr, line = unreachable_lines[priv_addr]
        lines.append(line)
        if args.toggles:
            edge_lines.append('  q{} -> g{} [label="toggle ref", {}];\n'.format(
                jsaddr, gobj_addr, UNREACHABLE_EDGE_STYLE))
        else:
            edge_lines.append('  q{} -> g{} [label="C reference", {}];\n'.format(
                jsaddr, gobj_addr, UNREACHABLE_EDGE_STYLE))
        rank_lines.append('  {{rank=same; q{}; g{};}}\n'.format(jsaddr,
                                                                gobj_addr))
    else:
        if args.toggles:
            arrow_dir = 'back' if lastref else 'both'
            edge_lines.append('  g{} -> q{} [label="toggle ref", dir={}, {}];\n'.format(
                gobj_addr, jsaddr, arrow_dir, JS_GOBJECT_EDGE_STYLE))
            rank_lines.append('  {{rank=same; g{}; q{};}}\n'.format(gobj_addr,
                                                                    jsaddr))
        else:
            edge_lines.append('  g{} -> q{} [label="trace", {}];\n'.format(
                gobj_addr, jsaddr, JS_GOBJECT_EDGE_STYLE))
            proxy_addr = proxies.get(jsaddr, None)
            if proxy_addr:
                edge_lines.append('  q{} -> g{} [label="C reference", {}];\n'.format(
                    proxy_addr, gobj_addr, C_REFERENCE_STYLE))
                rank_lines.append('  {{rank=same; g{}; q{}; q{};}}\n'.format(
                    gobj_addr, jsaddr, proxy_addr))


def process_postfile():
    with open(args.postfile, 'r') as post:
        line = post.readline()
        while line:
            lines.append(line)
            match = cedge_re.search(line)
            if match:
                gobj_addr, priv_addr = match.group(1, 2)
                is_lastref = '(1)' in line
                format_js_gobject_association(gobj_addr, priv_addr, is_lastref)
            line = post.readline()


def maybe_readd(lines, addr):
    if addr in unreachable_maybe_readd:
        lines.append(unreachable_maybe_readd[addr])
        del unreachable_maybe_readd[addr]


with open(args.dotfile, 'r') as dot:
    lines.append(dot.readline())  # 'digraph {'
    lines += defaults
    line = dot.readline()
    while line:
        match = proxy_re.search(line)
        if match:
            proxy_addr, proxy_target_addr = match.group(1, 2)
            proxies[proxy_target_addr] = proxy_addr
            # if the proxy was unreachable but has a live target, then put it
            # back into the list of nodes
            maybe_readd(lines, proxy_addr)
            # skip proxy->target edges, they go through the GObject and just
            # make the graph less legible
            line = dot.readline()
            continue

        if 'Unreachable' in line:
            # Unreachable node, omit by default; could be part of an edge that
            # we consider interesting, though, so save for later
            jsaddr = None
            match = jsaddr_re.search(line)
            if match:
                jsaddr = match.group(1)
                unreachable_maybe_readd[jsaddr] = line

            match = priv_addr_re.search(line)
            if match and jsaddr is not None:
                priv_addr = match.group(1)
                if priv_addr is not None:
                    unreachable_lines[priv_addr] = (jsaddr, line)

            line = dot.readline()
            continue

        match = jsedge_re.search(line)
        if match:
            privs[match.group(2)] = match.group(1)

        match = special_case_re.search(line)
        if match:
            source_addr, target_addr = match.group(1, 2)
            # if either is unreachable, put it back into the list of nodes
            maybe_readd(lines, source_addr)
            maybe_readd(lines, target_addr)

        if line[0] == '}':
            process_postfile()
            lines += edge_lines
            lines += rank_lines
        lines.append(line)
        line = dot.readline()

if not args.verbose:
    # The addresses are great for debugging and comparing to logs, but bad for
    # legibility of the graph in a presentation
    rm_addr_re = re.compile(r'(?:gobj|jsobj|priv)@0x[a-fA-F0-9]+')
    gobject_re = re.compile(r'\"(?:Garbage)?([a-zA-Z0-9_]+) (\([0-9]+\))\\n\\n\"')
    jsobject_re = re.compile(r'\"(Unreachable\\n)?GObject_Object\\n\\n\\n\\"([a-zA-Z0-9_ ]+)\\"\"')
    js_nolabel_re = re.compile(r'GObject_Object\\n\\n')
    expando_re = re.compile(r'\"(Unreachable\\n)?GObject_Expando\\n\\n\\n\\"([a-zA-Z0-9_ ]+)\\"\"')
    expando_nolabel_re = re.compile(r'GObject_Expando\\n\\n')
    for ix, line in enumerate(lines):
        line = rm_addr_re.sub('', line)
        line = gobject_re.sub(r'<GObject<br/><b>\1</b><br/>\2>', line)
        line = jsobject_re.sub(r'<\1JS wrapper<br/><b>\2</b>>', line)
        line = js_nolabel_re.sub('JS wrapper', line)
        line = expando_re.sub(r'<\1Expando<br/><b>\2</b>>', line)
        line = expando_nolabel_re.sub('Expando', line)
        line = line.replace('<Unreachable\\n', '<Unreachable<br/>')
        lines[ix] = line

with open(args.dotfile, 'w') as dot:
    for line in lines:
        dot.write(line)
