#pragma once
#include <glib-object.h>

G_BEGIN_DECLS

void garbage_dump(const char* filename);

#define GARBAGE_TYPE_CAN garbage_can_get_type()
G_DECLARE_FINAL_TYPE(GarbageCan, garbage_can, GARBAGE, CAN, GObject)
struct _GarbageCanClass {
    GObjectClass parent_class;
};

void garbage_can_add(GarbageCan* self, GObject* garbage);
void garbage_can_destroy(GarbageCan* self);

#define GARBAGE_TYPE_WINDOW garbage_window_get_type()
G_DECLARE_FINAL_TYPE(GarbageWindow, garbage_window, GARBAGE, WINDOW, GarbageCan)
struct _GarbageWindowClass {
    GarbageCanClass parent_class;
};

#define GARBAGE_TYPE_BOX1 garbage_box1_get_type()
G_DECLARE_FINAL_TYPE(GarbageBox1, garbage_box1, GARBAGE, BOX1, GarbageCan)
struct _GarbageBox1Class {
    GarbageCanClass parent_class;
};

#define GARBAGE_TYPE_LABEL1 garbage_label1_get_type()
G_DECLARE_FINAL_TYPE(GarbageLabel1, garbage_label1, GARBAGE, LABEL1, GarbageCan)
struct _GarbageLabel1Class {
    GarbageCanClass parent_class;
};

#define GARBAGE_TYPE_BOX2 garbage_box2_get_type()
G_DECLARE_FINAL_TYPE(GarbageBox2, garbage_box2, GARBAGE, BOX2, GarbageCan)
struct _GarbageBox2Class {
    GarbageCanClass parent_class;
};

#define GARBAGE_TYPE_LABEL2 garbage_label2_get_type()
G_DECLARE_FINAL_TYPE(GarbageLabel2, garbage_label2, GARBAGE, LABEL2, GarbageCan)
struct _GarbageLabel2Class {
    GarbageCanClass parent_class;
};

#define GARBAGE_TYPE_BUTTON garbage_button_get_type()
G_DECLARE_FINAL_TYPE(GarbageButton, garbage_button, GARBAGE, BUTTON, GarbageCan)
struct _GarbageButtonClass {
    GarbageCanClass parent_class;
};

G_END_DECLS
